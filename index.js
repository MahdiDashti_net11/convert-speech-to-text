window.SpeechRecognition = window.SpeechRecognition || window.webkitSpeechRecognition;
if (!window.SpeechRecognition) {
    alert('مرورگر شما قابلیت اجرا این برنامه را ندارد')
}
const recognition = new SpeechRecognition();

recognition.interimResults = true;
recognition.lang = 'fa-IR';


let p = document.createElement('p');
const words = document.querySelector('.words');
words.appendChild(p);

//The result event of the Web Speech API is fired when the speech recognition service returns a result — a word or phrase has been positively recognized and this has been communicated back to the app
recognition.addEventListener('result', e => {
    const transcript = Array.from(e.results)
        .map(result => result[0]).map(result => result.transcript).join('')
        p.textContent=transcript
    if (e.results[0].isFinal) {
        p = document.createElement('p');
        words.appendChild(p);
    }
})


recognition.addEventListener('end', recognition.start);
recognition.start();